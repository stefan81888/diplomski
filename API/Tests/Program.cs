﻿using System;
using System.Collections.Generic;
using System.Linq;
using Nest;

namespace Tests
{
    class Program
    {
		public static class Config
		{
			private static Uri _node;
			private static string _nodeUri;
			private static ConnectionSettings _settings;
			private static ElasticClient _client;

			public static ElasticClient GetClient()
			{
				//_nodeUri = "http://localhost:9200";
				_nodeUri = "http://elastic.dacha204.com:9200";
				_node = new Uri(_nodeUri);
				_settings = new ConnectionSettings(_node);
				_client = new ElasticClient(_settings);

				return _client;
			}
		}
		public class DataProvider
		{
			private Repository<Post> _postRepository;
			private Repository<Thread> _threadRepository;
			private Repository<Topic> _topicRepository;
			private Repository<Subforum> _subforumRepository;
			private Repository<User> _userRepository;
			private Repository<DirectMessage> _messageRepository;

			public DataProvider()
			{
				_postRepository = new Repository<Post>();
				_threadRepository = new Repository<Thread>();
				_topicRepository = new Repository<Topic>();
				_subforumRepository = new Repository<Subforum>();
				_userRepository = new Repository<User>();
				_messageRepository = new Repository<DirectMessage>();
			}

			public void AddPost(Post post)
			{
				post.Id = _postRepository.GenerateId();
				_postRepository.CreateOrUpdateDocument(post);
			}

			public void CreateSubforum(Subforum subforum)
			{
				subforum.Id = _subforumRepository.GenerateId();
				_subforumRepository.CreateOrUpdateDocument(subforum);
			}

			public void CreateThread(Thread thread)
			{
				thread.Id = _threadRepository.GenerateId();
				_threadRepository.CreateOrUpdateDocument(thread);
			}

			public void CreateTopic(Topic topic)
			{
				topic.Id = _topicRepository.GenerateId();
				_topicRepository.CreateOrUpdateDocument(topic);
			}

			public void CreateUser(User user)
			{
				user.Id = _userRepository.GenerateId();
				_userRepository.CreateOrUpdateDocument(user);
			}

			public List<DirectMessage> GetAllMessages(int userId)
			{
				List<DirectMessage> sent = SentMessages(userId);
				List<DirectMessage> received = ReceivedMessages(userId);

				List<DirectMessage> messages = sent;
				messages.AddRange(received);

				return messages;
			}

			private List<DirectMessage> SentMessages(int userId)
			{
				int max = _messageRepository.CountDocuments();
				List<DirectMessage> messages = new List<DirectMessage>();
				messages = _messageRepository.Search("sender", userId.ToString(), 0, max);

				return messages;
			}

			private List<DirectMessage> ReceivedMessages(int userId)
			{
				int max = _messageRepository.CountDocuments();
				List<DirectMessage> messages = new List<DirectMessage>();
				messages = _messageRepository.Search("receiver", userId.ToString(), 0, max);

				return messages;
			}

			public List<Post> GetPosts(int threadId)
			{
				int totalNumberOfDocs = _postRepository.CountDocuments();
				List<Post> result = _postRepository.Search("thread", threadId.ToString(), 0, totalNumberOfDocs);
				List<Post> sorted = result.OrderBy(x => x.Id).ToList();
				return sorted;
			}

			public List<Subforum> GetSubforums()
			{
				int max = _subforumRepository.CountDocuments();
				List<Subforum> result = _subforumRepository.Search("forum", "forum", 0, max);
				return result;
			}

			public List<Thread> GetThreads(int topicId)
			{
				int max = _messageRepository.CountDocuments();
				List<Thread> result = _threadRepository.Search("topic", topicId.ToString(), 0, max);
				return result;
			}

			public List<Topic> GetTopics(int subforumId)
			{
				int max = _messageRepository.CountDocuments();
				List<Topic> result = _topicRepository.Search("subforum", subforumId.ToString(), 0, max);
				return result;
			}

			public bool Login(User user)
			{
				User userFromDb = _userRepository.GetDocumentById(user.Id);

				if (userFromDb == null)
					return false;

				return user.Password == userFromDb.Password;
			}

			public void SendMessage(DirectMessage message)
			{
				message.Id = _messageRepository.GenerateId();
				_messageRepository.CreateOrUpdateDocument(message);
			}

			public List<Post> SearchPosts(string query)
			{
				return _postRepository.Search("content", query);
			}

			public List<User> SearchUsers(string query)
			{
				return _userRepository.Search("username", query);
			}

			public List<DirectMessage> SearchMessages(string query)
			{
				return _messageRepository.Search("content", query);
			}

			public List<Post> GetPostsOfAPage(int threadId, int pageNumber)
			{
				pageNumber--;
				List<Post> allPosts = GetPosts(threadId);
				int startIndex = pageNumber * 10;
				if (startIndex > allPosts.Count)
					return new List<Post>();
				int offset = 10;
				if (startIndex + offset > allPosts.Count)
					offset = allPosts.Count - startIndex;
				List<Post> result = allPosts.GetRange(startIndex, offset);
				return result;
			}

			public void RemovePost(Post post)
			{
				_postRepository.DeleteDocument(post);
			}

			public void RemoveThread(Thread thread)
			{
				_threadRepository.DeleteDocument(thread);
			}

			public void RemoveTopic(Topic topic)
			{
				_topicRepository.DeleteDocument(topic);
			}

			public void RemoveSubforum(Subforum subforum)
			{
				_subforumRepository.DeleteDocument(subforum);
			}

			public List<Thread> GetRecentThreads()
			{
				int max = _threadRepository.CountDocuments();
				List<Thread> allThreads = _threadRepository.Search("type", "thread", 0, max);
				List<Thread> sorted = allThreads.OrderBy(c => c.Id).ToList();
				int startIndex = sorted.Count - 5;
				if (startIndex < 0)
					startIndex = 0;
				int offset = 5;
				if (startIndex + offset > sorted.Count)
					offset = sorted.Count;
				return sorted.GetRange(startIndex, offset);
			}
		}	

		public class Repository<T> where T : class
		{
			private string _index;
			private ElasticClient _client;
			private string _type;

			public void DeleteAll()
			{
				_client.DeleteIndex(_index);
			}

			public Repository()
			{
				_type = "data";
				_index = "forum";
				_client = Config.GetClient();
			}

			public void DropIndex()
			{
				_client.DeleteIndex(_index);
			}
			public void CreateOrUpdateDocument(T document)
			{
				var index = _client.Index(document, i => i
				.Index(_index)
				.Type(_type));
			}

			public void DeleteDocument(T document)
			{
				_client.Delete(new DocumentPath<T>(document), i => i
				.Index(_index)
				.Type(_type));
			}

			public List<T> Search(string field, string query, int from = 0, int size = 10)
			{
				ISearchResponse<T> response = _client.Search<T>(s => s
				.Index(_index)
				.Type(_type)
				.Query(q => q
				.Match(m => m
					.Field(field)
					.Query(query))).From(from).Size(size));

				return response.Documents.ToList<T>();
			}

			public T GetDocumentById(int id)
			{
				return Search("_id", id.ToString()).First();
			}

			public List<T> MatchAll(int from = 0, int size = 10)
			{
				ISearchResponse<T> response = _client.Search<T>(s => s
				.Index(_index)
				.Type(_type)
				.Query(q => q
				.MatchAll()).From(from).Size(size));

				return response.Documents.ToList<T>();
			}

			public int GenerateId()
			{
				int id = 0;
				object sync = new object();
				lock (sync)
				{
					id = CountDocuments();
				}
				return id;
			}

			public int CountDocuments()
			{
				var res = _client.Count<T>(c => c.Index(_index).Type(_type));
				return (int)res.Count;
			}
		}
		public class DirectMessage
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public string Content { get; set; }
			public int Sender { get; set; }
			public int Receiver { get; set; }
		}
		public class Post
		{
			public int Id { get; set; }
			public string Content { get; set; }
			public int Thread { get; set; }
			public int User { get; set; }
		}
		public class Subforum
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public List<int> Topics { get; set; }
			public string Forum { get; set; }

			public Subforum()
			{
				Topics = new List<int>();
				Forum = "forum";
			}
		}
		public class Thread
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public int Topic { get; set; }
			public List<int> Posts { get; set; }
			public string Type { get; set; }

			public Thread()
			{
				Posts = new List<int>();
				Type = "thread";
			}
		}
		public class Topic
		{
			public int Id { get; set; }
			public string Title { get; set; }
			public int Subforum { get; set; }
			public List<int> Threads { get; set; }

			public Topic()
			{
				Threads = new List<int>();
			}
		}
		public class User
		{
			public int Id { get; set; }
			public string Username { get; set; }
			public DateTime DateOfBirth { get; set; }
			public int NumberOfPosts { get; set; }
			public bool IsMale { get; set; }
			public string Email { get; set; }
			public string Password { get; set; }
			public List<int> Posts { get; set; }

			public User()
			{
				Posts = new List<int>();
				Password = string.Empty;
			}
		}

		static void Main(string[] args)
		{
			var user = new User()
			{
				Username = "stef",
				Password = string.Empty
			};

			var thread = new Thread()
			{
				Title = "thread 67",
				Topic = 1
			};

			var topic = new Topic()
			{
				Title = "Topic title",
			};

			var subforum = new Subforum()
			{
				Title = "Subforum",
			};

			var post = new Post()
			{
				Content = "kewl ",
				Thread = 23
			};

			var dm = new DirectMessage()
			{
				Content = "Hello world",
				Sender = 10,
				Receiver = 1
			};

			var provider = new DataProvider();
			var repo = new Repository<Post>();
			repo.DeleteAll();
				
		}
	}
}
