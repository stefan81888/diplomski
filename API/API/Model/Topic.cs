﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Topic
    {
		public int Id { get; set; }
		public string Title { get; set; }
		public int Subforum { get; set; }
		public List<int> Threads { get; set; }
		public string Type { get; set; }

		public Topic()
		{
			Threads = new List<int>();
			Type = "topic";
		}
	}
}
