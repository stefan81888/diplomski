﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class DirectMessage
    {
		public int Id { get; set; }
		public string Title { get; set; }
		public string Content { get; set; }
		public int Sender { get; set; }
		public int Receiver { get; set; }
		public string Type { get; set; }

		public DirectMessage()
		{
			Type = "message";
		}
	}
}
