﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Post
    {
		public int Id { get; set; }
		public string Content { get; set; }
		public int Thread { get; set; }
		public int User { get; set; }
		public string Type { get; set; }

		public Post()
		{
			Type = "post";
		}
	}
}
