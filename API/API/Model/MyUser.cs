﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace API.Model
{
    public class MyUser
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int NumberOfPosts { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public List<int> Posts { get; set; }
        public string PictureUrl { get; set; }

        public MyUser()
		{
			Posts = new List<int>();
			Password = string.Empty;
		}
	}
}
