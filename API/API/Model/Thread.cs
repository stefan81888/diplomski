﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Thread
    {
		public int Id { get; set; }
		public string Title { get; set; }
		public int Topic { get; set; }
		public List<int> Posts { get; set;}
		public string Type { get; set; }

		public Thread()
		{
			Posts = new List<int>();
			Type = "thread";
		}
	}
}
