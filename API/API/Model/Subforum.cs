﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Model
{
    public class Subforum
    {
		public int Id { get; set; }
		public string Title { get; set; }
		public List<int> Topics { get; set; }
		public string Forum { get; set; }
		public string Type { get; set; }
		
		public Subforum()
		{
			Topics = new List<int>();
			Forum = "forum";
			Type = "subforum";
		}
	}
}
