﻿using API.DataProvider;
using API.Model;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace API.Controllers
{
	
	public class ForumsController : ControllerBase
	{
		private readonly IDataProvider _provider;

		public ForumsController(IDataProvider dataProvider)
		{
			_provider = dataProvider;
		}

	    [Route("posts/add")]
        [HttpOptions]
	    public IActionResult PostsOptions() => Ok();

        [Route("posts/add")]
		[HttpPost]
		public void AddPost([FromBody]Post post)
		{
			_provider.AddPost(post);
		}

		[Route("subforums/add")]
		[HttpPost]
		public void CreateSubforum([FromBody]Subforum subforum)
		{
			_provider.CreateSubforum(subforum);
		}

	    [Route("subforums/add")]
	    [HttpOptions]
	    public IActionResult CreateSubforumOk()
	    {
	        return Ok();
	    }

        [Route("threads/add")]
		[HttpPost]
		public void CreateThread([FromBody]Thread thread)
		{
			_provider.CreateThread(thread);
		}

	    [Route("threads/add")]
	    [HttpOptions]
	    public IActionResult CreateThreadOk()
	    {
	        return Ok();
	    }

        [Route("topics/add")]
		[HttpPost]
		public void CreateTopic([FromBody]Topic topic)
		{
			_provider.CreateTopic(topic);
		}

		[Route("users/add")]
		[HttpPost]
		public void CreateUser([FromBody]MyUser user)
		{
			_provider.CreateUser(user);
		}

		[Route("messages/{userId}")]
		[HttpGet]
		public List<DirectMessage> GetAllMessages([FromRoute]int userId)
		{
			return _provider.GetAllMessages(userId);
		}

		[Route("posts/{threadId}")]
		[HttpGet]
		public List<Post> GetPosts([FromRoute] int threadId)
		{
            var res = _provider.GetPosts(threadId);
		    return res;
		}

	    [Route("subforums")]
        [HttpGet]
		public List<Subforum> GetSubforums()
		{
			return _provider.GetSubforums();
		}

		[Route("topicthreads/{topicId}")]
		[HttpGet]
		public List<Thread> GetThreads([FromRoute]int topicId)
		{
            var res = _provider.GetTopicThreads(topicId);
		    return res;
		}

		[Route("subforumtopics/{subforumId}")]
		[HttpGet]
		public List<Topic> GetTopics([FromRoute]int subforumId)
		{
            var res = _provider.GetSubforumTopics(subforumId);
		    return res;
		}

	    [Route("topics/{topicId}")]
	    [HttpGet]
	    public Topic GetTopic([FromRoute]int topicId)
	    {
	        return _provider.GetTopic(topicId);
	    }

        [Route("users/login")]
		[HttpPost]
		public API.Model.MyUser Login([FromBody] JObject user)
        {
            return _provider.Login(JsonConvert.DeserializeObject<MyUser>(user.ToString()));
        }

	    [Route("topics/add")]
	    [HttpOptions]
	    public IActionResult AddTopicOk()
	    {
	        return Ok();
	    }

        [Route("users/register")]
	    [HttpPost]
	    public bool Register([FromBody] JObject user)
        {
            var res = _provider.Register(JsonConvert.DeserializeObject<MyUser>(user.ToString())).Id != -1;
            return res;
        }

	    [Route("users/register")]
	    [HttpOptions]
	    public IActionResult RegisterOk()
	    {
	        return Ok();
	    }

        [Route("users/login")]
	    [HttpOptions]
	    public IActionResult LoginOk() => Ok();

        [Route("messages/send")]
		[HttpPost]
		public void SendMessage([FromBody]DirectMessage message)
		{
			_provider.SendMessage(message);
		}

		[Route("posts/search/{query}")]
		[HttpGet]
		public List<Post> SearchPosts([FromRoute]string query)
		{
			return _provider.SearchPosts(query);
		}

		[Route("users/search/{query}")]
		[HttpGet]
		public List<MyUser> SearchUsers([FromRoute]string query)
		{
			return _provider.SearchUsers(query);
		}

	    [Route("users/{id}")]
	    [HttpGet]
	    public MyUser GetUserById([FromRoute]int id)
	    {
	        return _provider.GetUserById(id);
	    }

        [Route("messages/search/{query}")]
		[HttpGet]
		public List<DirectMessage> SearchMessages([FromRoute]string query)
		{
		    var res = _provider.SearchMessages(query);
		    return res;
		}

		[Route("posts/page/{threadId}/{pageNumber}")]
		[HttpGet]
		public List<Post> GetPostsOfAPage([FromRoute]int threadId, [FromRoute]int pageNumber)
		{
            var res = _provider.GetPostsOfAPage(threadId, pageNumber);
		    return res;
		}

		[Route("posts/remove")]
		[HttpDelete]
		public void RemovePost([FromBody]Post post)
		{
			_provider.RemovePost(post);
		}

		[Route("threads/remove")]
		[HttpDelete]
		public void RemoveThread([FromBody]Thread thread)
		{
			_provider.RemoveThread(thread);
		}

		[Route("topics/remove")]
		[HttpDelete]
		public void RemoveTopic([FromBody]Topic topic)
		{
			_provider.RemoveTopic(topic);
		}

		[Route("subforums/remove")]
		[HttpDelete]
		public void RemoveSubforum([FromBody]Subforum subforum)
		{
			_provider.RemoveSubforum(subforum);
		}

		[Route("threads/recent")]
		[HttpGet]
		public List<Thread> GetRecentThreads()
		{
            var res = _provider.GetRecentThreads();
		    return res;
		}
    }
}