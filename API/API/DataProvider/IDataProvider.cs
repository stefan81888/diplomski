﻿using API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.DataProvider
{
	public interface IDataProvider
	{
		void AddPost(Post post);
		List<Post> GetPosts(int threadId);
		void CreateThread(Thread thread);
		List<Thread> GetTopicThreads(int topicId);
		void CreateTopic(Topic topic);
		List<Topic> GetSubforumTopics(int subforumId);
		void CreateSubforum(Subforum subforum);
		List<Subforum> GetSubforums();
		void SendMessage(DirectMessage message);
		void CreateUser(MyUser user);
		MyUser Login(MyUser user);
	    MyUser Register(MyUser user);
		List<DirectMessage> GetAllMessages(int userId);
		List<Post> SearchPosts(string query);
		List<MyUser> SearchUsers(string query);
		List<DirectMessage> SearchMessages(string query);
		List<Post> GetPostsOfAPage(int threadId, int pageNumber);
		void RemovePost(Post post);
		void RemoveThread(Thread thread);
		void RemoveTopic(Topic topic);
		void RemoveSubforum(Subforum subforum);
		List<Thread> GetRecentThreads();
	    Topic GetTopic(int topicId);
        MyUser GetUserById(int id);
    }
}
