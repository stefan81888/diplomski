﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;

namespace API.DataProvider
{
    public static class Config
    {
		private static Uri _node;
		private static string _nodeUri;
		private static ConnectionSettings _settings;
		private static ElasticClient _client;

		public static ElasticClient GetClient()
		{
			//_nodeUri = "http://localhost:9200";
			_nodeUri = "http://elastic.dacha204.com:9200";
			_node = new Uri(_nodeUri);
			_settings = new ConnectionSettings(_node);
			_client = new ElasticClient(_settings);

			return _client;
		}
	}
}
