﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nest;
using API.DataProvider;

namespace API.DataProvider
{
    public class Repository<T> where T: class
    {
		private string _index;
		private ElasticClient _client;
		private string _type;

		public Repository()
		{
			_type = "data";
			_index = "forum";
			_client = Config.GetClient();
		}

		public void CreateOrUpdateDocument(T document)
		{
			var index = _client.Index(document, i => i
			.Index(_index)
			.Type(_type));
		}		

		public void DeleteDocument(T document)
		{
			_client.Delete(new DocumentPath<T>(document), i => i
			.Index(_index)
			.Type(_type));
		}

		public List<T> Search(string field, string query, int from = 0, int size = 10)
		{
			ISearchResponse<T> response = _client.Search<T>(s => s
			.Index(_index)
			.Type(_type)
			.Query(q => q
			.Match(m => m
				.Field(field)
				.Query(query))).From(from).Size(size));

			return response.Documents.ToList<T>();
		}

		public T GetDocumentById(int id)
		{
			return Search("_id", id.ToString()).First();
		}

		public List<T> MatchAll(int from = 0, int size = 10)
		{
			ISearchResponse<T> response = _client.Search<T>(s => s
			.Index(_index)
			.Type(_type)
			.Query(q => q
			.MatchAll()).From(from).Size(size));

			return response.Documents.ToList<T>();
		}

		public int GenerateId()
		{
			int id = 0;
			object sync = new object();
			lock (sync)
			{
				id = CountDocuments();
			}
			return id;
		}

		public int CountDocuments()
		{
			var res = _client.Count<T>(c => c.Index(_index).Type(_type));
			return (int)res.Count;
		}
	}
}
