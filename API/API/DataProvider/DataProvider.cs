﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Model;

namespace API.DataProvider
{
	public class DataProvider : IDataProvider
	{
		private Repository<Post> _postRepository;
		private Repository<Thread> _threadRepository;
		private Repository<Topic> _topicRepository;
		private Repository<Subforum> _subforumRepository;
		private Repository<MyUser> _userRepository;
		private Repository<DirectMessage> _messageRepository;

		public DataProvider()
		{
			_postRepository = new Repository<Post>();
			_threadRepository = new Repository<Thread>();
			_topicRepository = new Repository<Topic>();
			_subforumRepository = new Repository<Subforum>();
			_userRepository = new Repository<MyUser>();
		    _messageRepository = new Repository<DirectMessage>();
	}

		public void AddPost(Post post)
		{
			post.Id = _postRepository.GenerateId();
			_postRepository.CreateOrUpdateDocument(post);
		    MyUser user = _userRepository.GetDocumentById(post.User);
            user.Posts.Add(post.Id);
		    user.NumberOfPosts++;
            _userRepository.CreateOrUpdateDocument(user);
		    Thread thread = _threadRepository.GetDocumentById(post.Thread);
            thread.Posts.Add(post.Id);
            _threadRepository.CreateOrUpdateDocument(thread);
		}

		public void CreateSubforum(Subforum subforum)
		{
			subforum.Id = _subforumRepository.GenerateId();
			_subforumRepository.CreateOrUpdateDocument(subforum);
		}

		public void CreateThread(Thread thread)
		{
			thread.Id = _threadRepository.GenerateId();
			_threadRepository.CreateOrUpdateDocument(thread);
		}

		public void CreateTopic(Topic topic)
		{
			topic.Id = _topicRepository.GenerateId();
			_topicRepository.CreateOrUpdateDocument(topic);
		    Subforum subforum = _subforumRepository.GetDocumentById(topic.Subforum);
            subforum.Topics.Add(topic.Id);
            _subforumRepository.CreateOrUpdateDocument(subforum);
		}

		public void CreateUser(MyUser user)
		{
			user.Id = _userRepository.GenerateId();
			_userRepository.CreateOrUpdateDocument(user);
		}

		public List<DirectMessage> GetAllMessages(int userId)
		{
			List<DirectMessage> sent = SentMessages(userId);
			List<DirectMessage> received = ReceivedMessages(userId);

			List<DirectMessage> messages = sent;
			messages.AddRange(received);

			return messages;
		}

		private List<DirectMessage> SentMessages(int userId)
		{
			int max = _messageRepository.CountDocuments();
			List<DirectMessage> messages = new List<DirectMessage>();
			messages = _messageRepository.Search("sender", userId.ToString(), 0, max);

			return messages;
		}

		private List<DirectMessage> ReceivedMessages(int userId)
		{
			int max = _messageRepository.CountDocuments();
			List<DirectMessage> messages = new List<DirectMessage>();
			messages = _messageRepository.Search("receiver", userId.ToString(), 0, max);

			return messages;
		}

		public List<Post> GetPosts(int threadId)
		{
			int totalNumberOfDocs= _postRepository.CountDocuments();
			List<Post> result = _postRepository.Search("thread", threadId.ToString(), 0, totalNumberOfDocs);
			List<Post> sorted = result.OrderBy(x => x.Id).ToList();
			return sorted;
		}

		public List<Subforum> GetSubforums()
		{
			int max = _subforumRepository.CountDocuments();
			List<Subforum> result = _subforumRepository.Search("forum", "forum", 0, max);
			return result;
		}

		public List<Thread> GetTopicThreads(int topicId)
		{
			int max = _threadRepository.CountDocuments();
			List<Thread> result = _threadRepository.Search("topic", topicId.ToString(), 0, max);
			return result;
		}

		public List<Topic> GetSubforumTopics(int subforumId)
		{
			int max = _topicRepository.CountDocuments();
			List<Topic> result = _topicRepository.Search("subforum", subforumId.ToString(), 0, max);
			return result;
		}

		public MyUser Login(MyUser user)
		{
		    MyUser userFromDb = _userRepository.Search("username", user.Username).FirstOrDefault();

			if (userFromDb == null)
				return new MyUser(){Id = -1};

			if(user.Password == userFromDb.Password)
				return userFromDb;

			return new MyUser() { Id = -1 };
		}

		public void SendMessage(DirectMessage message)
		{
			message.Id = _messageRepository.GenerateId();
			_messageRepository.CreateOrUpdateDocument(message);
		}

		public List<Post> SearchPosts(string query)
		{
			return _postRepository.Search("content", query);
		}

		public List<MyUser> SearchUsers(string query)
		{
			return _userRepository.Search("username", query);
		}

		public List<DirectMessage> SearchMessages(string query)
		{
			return _messageRepository.Search("content", query);
		}

		public List<Post> GetPostsOfAPage(int threadId, int pageNumber)
		{
            if(pageNumber < 1)
                return new List<Post>();
			pageNumber--;
			List<Post> allPosts = GetPosts(threadId);
			int startIndex = pageNumber * 10;
			if (startIndex > allPosts.Count)
				return new List<Post>();
			int offset = 10;
			if (startIndex + offset > allPosts.Count)
				offset = allPosts.Count - startIndex;
			List<Post> result = allPosts.GetRange(startIndex, offset);
			return result;
		}

		public void RemovePost(Post post)
		{
			_postRepository.DeleteDocument(post);
		}

		public void RemoveThread(Thread thread)
		{
			_threadRepository.DeleteDocument(thread);
		}

		public void RemoveTopic(Topic topic)
		{
			_topicRepository.DeleteDocument(topic);
		}

		public void RemoveSubforum(Subforum subforum)
		{
			_subforumRepository.DeleteDocument(subforum);
		}

		public List<Thread> GetRecentThreads()
		{
			int max = _threadRepository.CountDocuments();
			List<Thread> allThreads = _threadRepository.Search("type", "thread", 0, max);
			List<Thread> sorted = allThreads.OrderBy(c => c.Id).ToList();
			int startIndex = sorted.Count - 5;
			if (startIndex < 0)
				startIndex = 0;
			int offset = 5;
			if (startIndex + offset > sorted.Count)
				offset = sorted.Count;
			return sorted.GetRange(startIndex, offset);
		}

        public Topic GetTopic(int topicId)
        {
            return _topicRepository.GetDocumentById(topicId);
        }

        public MyUser GetUserById(int id)
        {
            return _userRepository.GetDocumentById(id);
        }

        public MyUser Register(MyUser user)
        {
            List<MyUser>  foundUsers = new List<MyUser>();
            MyUser emailOrUsernameExist = new MyUser(){Id = -1};

            foundUsers = _userRepository.Search("username", user.Username);
            if (foundUsers.Count > 0)
                return emailOrUsernameExist;

            foundUsers = _userRepository.Search("email", user.Email);
            if (foundUsers.Count > 0)
                return emailOrUsernameExist;

            user.Id = _userRepository.GenerateId();
            _userRepository.CreateOrUpdateDocument(user);

            return user;
        }
    }
}
