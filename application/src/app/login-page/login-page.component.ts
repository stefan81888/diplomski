import { Component, OnInit } from '@angular/core';
import { DataService } from '@app/app/services/implementation/dataService';
import { User } from '@app/app/data-model';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  username: string;
  password: string;
  status: string = "";
  statusSucces: string = ": Login successful!";
  statusFail: string = ": Failed to login.";

  constructor(private dataService: DataService) { }

  ngOnInit() {
  }

  login(){
   var user: User = {
    id: 0,
    email: "",
    numberOfPosts: 0,
    password: this.password,
    pictureUrl: null,
    posts: null,
    username: this.username
   };
   this.dataService.login(user).subscribe(() => {
    this.status = this.dataService.isLogged()? this.statusSucces : this.statusFail;
   });
}

}
