import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleSubforumComponent } from './single-subforum.component';

describe('SingleSubforumComponent', () => {
  let component: SingleSubforumComponent;
  let fixture: ComponentFixture<SingleSubforumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleSubforumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleSubforumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
