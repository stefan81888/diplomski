import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Subforum} from 'src/app/data-model/subforum';
import {Topic} from 'src/app/data-model/topic';
import {DataService} from 'src/app/services/implementation/dataService';

@Component({
  selector: 'app-single-subforum',
  templateUrl: './single-subforum.component.html',
  styleUrls: ['./single-subforum.component.css']
})
export class SingleSubforumComponent implements OnInit {

  topics: Topic[];
  id: number;
  title: string;

  constructor(private route: ActivatedRoute, private dataService: DataService) { }

  ngOnInit() {
    this.route.params.subscribe( params => {
      console.log(params);
      this.id = params['id'];
      this.title = params['title'];
      this.dataService.getSubforumTopics(this.id).subscribe(
        (data: Topic[]) => {
          this.topics = data;
        }, (err) => console.log(err)
      );
    } );
  
  }

}
