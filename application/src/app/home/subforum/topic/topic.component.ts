import { Component, OnInit, Input } from '@angular/core';
import {Topic} from 'src/app/data-model/topic';
import {Thread} from 'src/app/data-model/thread';
import { ActivatedRoute } from '@angular/router';
import {DataService} from 'src/app/services/implementation/dataService';


@Component({
  selector: 'app-topic',
  templateUrl: './topic.component.html',
  styleUrls: ['./topic.component.css']
})
export class TopicComponent implements OnInit {

  threads: Thread[];
  @Input() title: string;
  id: number;

  constructor(private route: ActivatedRoute, private dataService: DataService) {
    
   }

   isLogged(): boolean{
    var user = this.dataService.getLoggedUser();
    var res: boolean = user !== null;
    return res;
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      this.threads = [];
      this.id = params['id'];
      this.title = params['title'];
      this.dataService.getTopicThreads(this.id).subscribe(
        (data: Thread[]) => this.threads = data
      );
    });
  }

}
