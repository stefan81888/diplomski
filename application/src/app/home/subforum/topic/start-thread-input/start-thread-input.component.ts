import { Component, OnInit } from '@angular/core';
import { Thread } from 'src/app/data-model/thread';
import { DataService } from 'src/app/services/implementation/dataService';
import { ActivatedRoute, Router } from '@angular/router';
import { Post } from '../../../../data-model/post';
import { BehaviorSubject } from 'rxjs';


@Component({
  selector: 'app-start-thread-input',
  templateUrl: './start-thread-input.component.html',
  styleUrls: ['./start-thread-input.component.css']
})
export class StartThreadInputComponent implements OnInit {

  initialPost: string= "";
  title: string = "";
  command: string = "";
  topicId: BehaviorSubject<number> = new BehaviorSubject(null);
  

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) {
  
   }

  startThread(){
    var idOfTopic = this.topicId.value;
    var thread: Thread = {
      id: 0,
      posts: [],
      title: this.title,
      topic: idOfTopic,
      type: "thread"      
    };

    this.dataService.addThread(thread);
    this.router.navigate(['']);
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
        var id = params['topicId'];
        this.topicId.next(id);
    });
  }

}
