import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StartThreadInputComponent } from './start-thread-input.component';

describe('StartThreadInputComponent', () => {
  let component: StartThreadInputComponent;
  let fixture: ComponentFixture<StartThreadInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StartThreadInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StartThreadInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
