import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/data-model/post';
import { DataService } from 'src/app/services/implementation/dataService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-thread-posts',
  templateUrl: './thread-posts.component.html',
  styleUrls: ['./thread-posts.component.css']
})
export class ThreadPostsComponent implements OnInit {

  posts: Post[];
  lastPagePosts: boolean = true;
  title: string;
  threadId: number;
  pageNumber: number = 1;

  constructor(private route: ActivatedRoute, private dataService: DataService) {  }

  loadPosts(): void{
    this.route.params.subscribe( params => {
      this.title = params['title'];
      this.threadId = params['id'];
      this.dataService.getThreadPostsOnPage(this.pageNumber, this.threadId).subscribe(
        (data: Post[]) => {
         this.posts = data;
         if(this.posts === [] || this.posts.length < 10)
          this.lastPagePosts = false;
      });
    });    
  }  
  
  showNextButton(): boolean {

    return this.lastPagePosts;
  }

  showPrevButton(): boolean {
    return this.pageNumber > 1;
  }
  
  nextPage(): void{
    this.pageNumber++;
    this.loadPosts();
  }

  previousPage(): void{
    this.pageNumber--;
    this.loadPosts();
  }

  addPost($event: Post){
  this.posts.push($event);
  }

 isLogged(): boolean{
   var user = this.dataService.getLoggedUser();
   var res: boolean = user !== null;
   return res;
 }

  ngOnInit() {
    this.loadPosts();
  }

}
