import { Component, OnInit, Input } from '@angular/core';
import { Post } from 'src/app/data-model/post';
import { DataService } from 'src/app/services/implementation/dataService';
import { User } from '../../../../../data-model/user';

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.css']
})
export class SinglePostComponent implements OnInit {

  @Input() post: Post;
  postsCount: number = 0;
  profilePicURL: string;
  username: string = "";

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    console.log(this.post.user);
    this.dataService.getUserById(this.post.user).subscribe(
      (data: User) => {
        this.postsCount = data.numberOfPosts;
        this.username = data.username;
        this.profilePicURL = data.pictureUrl;                  
      }
    );
  }

}
