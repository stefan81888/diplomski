import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThreadPostsComponent } from './thread-posts.component';

describe('ThreadPostsComponent', () => {
  let component: ThreadPostsComponent;
  let fixture: ComponentFixture<ThreadPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThreadPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThreadPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
