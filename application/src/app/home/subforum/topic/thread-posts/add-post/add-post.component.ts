import { Component, OnInit,Output,  Input,EventEmitter } from '@angular/core';
import { DataService } from 'src/app/services/implementation/dataService';
import { Post } from 'src/app/data-model/post';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit {

  @Input() 
  threadId: number;
  content: string; 
  @Output() 
  postAdded: EventEmitter<Post> = new EventEmitter();

  constructor(private dataService: DataService) { }

  post(){
    var user = this.dataService.getLoggedUser();
    if(user === null)
     return;
    var userId: number = user.id;
    let post: Post = {content: this.content,  id: 0, thread: this.threadId, type:"post", user: userId};   
    this.dataService.addPost(post);
    this.content = "";
    this.postAdded.emit(post);
  }

  ngOnInit() {
  }

}
