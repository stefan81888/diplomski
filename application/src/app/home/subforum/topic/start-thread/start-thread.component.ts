import { Component, OnInit, Input } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-start-thread',
  templateUrl: './start-thread.component.html',
  styleUrls: ['./start-thread.component.css']
})
export class StartThreadComponent implements OnInit {

  command: string = "startThread";
  @Input()
  topicId: number;

  constructor(private router: Router) { 
  }

  startThreadInput(){
    this.router.navigate(['start-thread', this.topicId]);
  }

  ngOnInit() {
    console.log(this.topicId);
  }

}
