import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/implementation/dataService';
import {Topic} from 'src/app/data-model/topic';
import {Subforum} from 'src/app/data-model/subforum';
import { Router } from '../../../../node_modules/@angular/router';

@Component({
  selector: 'app-subforum',
  templateUrl: './subforum.component.html',
  styleUrls: ['./subforum.component.css']
})
export class SubforumComponent implements OnInit {
 
  @Input() subforum: Subforum;
  topics: Topic[];
  addTopic: boolean = false;
  addedTopicTitle: string = "";
  constructor(private dataService: DataService, private router: Router) {
   
   }

   enableAddTopic(){
     this.addTopic = true;
   }

   showAddTopicButton(): boolean{
     return this.dataService.isLogged();
   }

   addTopicToSubforum(event): void{
     if(event.key !== "Enter")
      return;

      var topic: Topic = {
        id: 0,
        threads: [],
        title: this.addedTopicTitle,
        type: "topic",
        subforum: this.subforum.id
      }
      
      this.dataService.addTopic(topic);
      this.addedTopicTitle = "";
      this.addTopic = false;

      //this.router.navigate(["single-subforum", this.subforum.title, this.subforum.id]);
   }

  ngOnInit() {
    this.topics = [];   
    this.dataService.getSubforumTopics(this.subforum.id).subscribe(
      (data: Topic[]) => {
        this.topics = data;
      }, (err) => console.log(err)
    );
  }

}
