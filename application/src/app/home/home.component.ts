import { Component, OnInit, OnChanges } from '@angular/core';
import {Subforum} from '../data-model/subforum';
import { DataService } from 'src/app/services/implementation/dataService';
import {Post} from 'src/app/data-model/post';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnChanges {

  subforums : Subforum[];
  addSubforumFlag: boolean = false;
  addedSubforumTitle: string = "";

  constructor(private dataService: DataService, private router: Router) { 
   
  }

  addSubforum(event): void{
    if(event.key !== "Enter")
     return;

     var subforum: Subforum = {
       forum: "forum",
       id: 0,
       title: this.addedSubforumTitle,
       topics: [],
       type: "subforum"
     }
     this.addedSubforumTitle = "";
     this.addSubforumFlag = false;
     
     this.dataService.addSubforum(subforum);
     this.dataService.getSubforums().subscribe((data) => {
      this.subforums = data;
      this.router.navigate([""]);
      });
   
     
  }

  isLogged():boolean{
    return this.dataService.isLogged();
  }

  showAddSubforumInput(){
    this.addSubforumFlag = true;
  }

  ngOnInit() {
    this.subforums = [];
    this.dataService.getSubforums().subscribe((data) => {
          this.subforums = data;
     });
  }

  ngOnChanges(){  
  }

}
