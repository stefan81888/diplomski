import { Component, OnInit } from '@angular/core';
import { Thread } from 'src/app/data-model/thread';
import { DataService } from 'src/app/services/implementation/dataService';

@Component({
  selector: 'app-recent-threads',
  templateUrl: './recent-threads.component.html',
  styleUrls: ['./recent-threads.component.css']
})
export class RecentThreadsComponent implements OnInit {

  recentThreads : Thread[];

  constructor(private dataService: DataService) { 
     dataService.getRecentThreads().subscribe(
       (data: Thread[]) => this.recentThreads = data
      );
  }

  ngOnInit() {
  }

}
