import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import {Post} from '@app/app/data-model'
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/services/implementation/dataService';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  postsResult: Post[];
  query: string;

  constructor(private dataService: DataService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.query = params['query'];
      this.dataService.seacrhPosts(this.query).subscribe(
        (data: Post[]) => {
          this.postsResult = data;
        }
      );
    });
  }

}
