import { Subforum } from "src/app/data-model/subforum";
import { Topic } from "src/app/data-model/topic";
import { Thread } from "src/app/data-model/thread";
import { Post } from "src/app/data-model/post";
import { DirectMessage } from "src/app/data-model/direct-message";
import {Observable} from  'rxjs';
import { User } from "../../data-model/user";

export interface IDataService{    
     getSubforums(): Observable<Subforum[]>;
     getSubforumTopics(subforumId: number):Observable<Topic[]>; 
     getRecentThreads(): Observable<Thread[]>; 
     getTopicThreads(topicId: number): Observable<Thread[]>;
     getThreadPostsOnPage(pageNumber: number, threadId: number): Observable<Post[]>;
     getUserById(id: number) : Observable<User>;
     addPost(post: Post): void;
     getLoggedUser(): User;
     login(user: User): Observable<User>;
     addThread(thread: Thread): void;
     logout(): void;
     seacrhPosts(query: string): Observable<Post[]>;
     getMessages(userId: number): Observable<DirectMessage[]>;
     register(user: User) : Observable<boolean>;
     isLogged() : boolean;
     addTopic(topic: Topic): void;
     addSubforum(subforum: Subforum): void;
}