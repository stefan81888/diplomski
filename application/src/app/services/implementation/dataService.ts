import { IDataService } from '../interfaces/iDataService';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { environment } from '@app/environments/environment'
import { User, Subforum, Topic, Post, Thread, DirectMessage } from '@app/app/data-model';

@Injectable()
export class DataService implements IDataService {
    private apiUrl: string = environment.server;
    private loggedUser: BehaviorSubject<User> = new BehaviorSubject(null);

    public constructor(private http: HttpClient) {    
     
    }

    getSubforums(): Observable<Subforum[]> {
        var url = this.apiUrl + "subforums";
        var result = this.http.get<Subforum[]>(url);
        return result;
    }

    getSubforumTopics(subforumId: number): Observable<Topic[]> {
        var url = this.apiUrl + "subforumtopics/" + subforumId;
        return this.http.get<Topic[]>(url);
    }

    getRecentThreads(): Observable<Thread[]> {
        var url = this.apiUrl + "threads/recent";
        return this.http.get<Thread[]>(url);
    }

    getTopicThreads(topicId: number): Observable<Thread[]> {
        var url = this.apiUrl + "topicthreads/" + topicId;
        return this.http.get<Thread[]>(url);
    }

    getThreadPostsOnPage(pageNumber: number, threadId: number): Observable<Post[]> {
        var url = this.apiUrl + "posts/page/" + threadId + "/" + pageNumber;
        return this.http.get<Post[]>(url);
    }

    getUserById(id: number): Observable<User> {
        var url = this.apiUrl + "users/" + id;
        return this.http.get<User>(url);
    }

    addPost(post: Post): void {
        var url = this.apiUrl + "posts/add";
        this.http.post<Post>(url, post).subscribe(
            () => { }
        );
    }

    getLoggedUser(): User {
        return this.loggedUser.value;
    }

    login(user: User): Observable<User> {
        var url = this.apiUrl + "users/login";

        return this.http.post<User>(url, user)
            .pipe(
                tap(u => {
                    this.loggedUser.next(u);
                })
            )
    }

    addThread(thread: Thread): void{
        var url = this.apiUrl + "threads/add";
        this.http.post<Post>(url, thread).subscribe(
            () => { }
        );
    }

    logout(): void{
        if(!this.isLogged())
            return;
        this.loggedUser = new BehaviorSubject(null);
    }

    seacrhPosts(query: string): Observable<Post[]>{
        var url = this.apiUrl + "posts/search/" + query;
        return this.http.get<Post[]>(url);
    }

    getMessages(userId: number): Observable<DirectMessage[]>{
        var url = this.apiUrl + "messages/" + userId;
        return this.http.get<DirectMessage[]>(url);
    }

    register(user: User) : Observable<boolean>{
        var url = this.apiUrl + "users/register";
        return this.http.post<boolean>(url, user)
    }

    isLogged() : boolean{
        return this.getLoggedUser() !== null;
    }

    addTopic(topic: Topic): void{
        var url = this.apiUrl + "topics/add";
        this.http.post<Topic>(url, topic).subscribe(
            () => { }
        );
    }

    addSubforum(subforum: Subforum): void{
        var url = this.apiUrl + "subforums/add";
        this.http.post<Subforum>(url, subforum).subscribe(
            () => { }
        );
    }

}