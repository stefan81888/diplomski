
export class User {
    public id: number;
    public numberOfPosts: number;
    public username: string;
    public posts: number[];
    public email: string;
    public password: string;
    public pictureUrl: string;

    public constructor() {
        this.posts = [];
    }
}