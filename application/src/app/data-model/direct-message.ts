export class DirectMessage{
    public id: number;
    public title: string;
    public content: string;
    public sender: number;
    public receiver: number;
    public type: string;

    public constructor()
    {
        this.type = "message";
    }
}