
export class Thread{
    public id: number;    
    public title: string;
    public topic: number;
    public posts: number[];
    public type: string;
    
        public constructor(){
             this.type = "thread";
             this.posts = [];
        }

}