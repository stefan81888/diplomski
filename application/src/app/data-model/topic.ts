
export class Topic{
    public id: number;    
    public title: string;
    public threads: number[];
    public subforum: number;
    public type: string;
    
        public constructor(){
             this.type = "thread";
             this.threads = [];
        }
}