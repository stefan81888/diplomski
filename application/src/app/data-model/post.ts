
export class Post{
    public id: number;
    public content: string;
    public user: number;
    public thread: number;
    public type: string;

    constructor() {
        this.type = "post";
    }
}