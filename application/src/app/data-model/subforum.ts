
export class Subforum{
    public id: number;
    public title: string;
    public topics: number[];
    public forum: string;
    public type: string;

    public constructor(){
         this.topics = [];
         this.forum = "forum";
         this.type = "subforum";
    }
    
}