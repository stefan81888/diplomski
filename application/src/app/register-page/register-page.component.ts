import { Component, OnInit } from '@angular/core';
import { DataService } from '@app/app/services/implementation/dataService';
import { User } from '@app/app/data-model';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit {

  username: string = "";
  password: string = "";
  pictureUrl: string = "";
  email: string = "";
  registerStatusMessage: string = "";
  succesMessage: string = ": Success! Please login to continue.";
  failMessage: string = ": User with this username or email already exist.";
  

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

  register(){

    if(this.username === "" || this.password === "" || this.pictureUrl === "" || this.email === ""){
      return;
    }

    var user: User = {
      email: this.email,
      id: 0,
      numberOfPosts: 0,
      password: this.password,
      pictureUrl: this.pictureUrl,
      posts: [],
      username: this.username
    }

    this.username = "";
    this.password = "";
    this.pictureUrl = "";
    this.email = "";

    this.dataService.register(user).subscribe(
      (data: boolean) => {
        if(data){
          this.registerStatusMessage = this.succesMessage;
         }
        else this.registerStatusMessage = this.failMessage;
      }
    );
  }
}
