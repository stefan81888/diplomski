import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleUserMessagesComponent } from './single-user-messages.component';

describe('SingleUserMessagesComponent', () => {
  let component: SingleUserMessagesComponent;
  let fixture: ComponentFixture<SingleUserMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SingleUserMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleUserMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
