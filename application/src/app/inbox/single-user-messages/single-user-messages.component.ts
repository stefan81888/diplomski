import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/data-model/post';
import { DataService } from 'src/app/services/implementation/dataService';

@Component({
  selector: 'app-single-user-messages',
  templateUrl: './single-user-messages.component.html',
  styleUrls: ['./single-user-messages.component.css']
})
export class SingleUserMessagesComponent implements OnInit {

  messages: Post[];
  
    constructor(private dataService: DataService) {
      //this.messages = this.dataService.getMessages("this topic");
    }
  

  ngOnInit() {
  }

}
