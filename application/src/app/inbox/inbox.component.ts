import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/data-model/post';
import { DataService } from 'src/app/services/implementation/dataService';
import {Router} from "@angular/router";


@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.css']
})
export class InboxComponent implements OnInit {

  messages: Post[];
  command: string = "messages";
  
  composeNew(){
    this.router.navigate(['start-thread', 'messages']);    
  }

  constructor(private dataService: DataService, private router: Router) {
    
   }

  ngOnInit() {
  }

}
