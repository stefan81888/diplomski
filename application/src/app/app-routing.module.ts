import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { InboxComponent } from './inbox/inbox.component';
import { SubforumComponent } from 'src/app/home/subforum/subforum.component';
import { SingleSubforumComponent } from 'src/app/home/subforum/single-subforum/single-subforum.component';
import { TopicComponent } from 'src/app/home/subforum/topic/topic.component';
import { StartThreadInputComponent } from 'src/app/home/subforum/topic/start-thread-input/start-thread-input.component';
import { ThreadPostsComponent } from 'src/app/home/subforum/topic/thread-posts/thread-posts.component';
import { SingleUserMessagesComponent } from 'src/app/inbox/single-user-messages/single-user-messages.component';
import {SearchResultsComponent} from 'src/app/search-results/search-results.component'
import {LoginPageComponent} from 'src/app/login-page/login-page.component'
import {RegisterPageComponent} from 'src/app/register-page/register-page.component'


const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'subforum', component: SubforumComponent },   
  { path: 'single-subforum/:title/:id', component: SingleSubforumComponent },      
  { path: 'topic/:title/:id', component: TopicComponent },
  { path: 'start-thread/:topicId', component: StartThreadInputComponent },
  { path: 'thread-posts/:title/:id', component: ThreadPostsComponent },  
  { path: 'single-user-messages', component: SingleUserMessagesComponent },
  { path: 'search-results/:query', component: SearchResultsComponent },
  { path: 'login', component: LoginPageComponent },
  { path: 'register', component: RegisterPageComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}