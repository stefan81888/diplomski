import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ProfileComponent } from './profile/profile.component';
import { AppRoutingModule } from './/app-routing.module';
import { InboxComponent } from './inbox/inbox.component';
import { SubforumComponent } from './home/subforum/subforum.component';
import { TopicComponent } from './home/subforum/topic/topic.component';
import { RecentThreadsComponent } from './home/recent-threads/recent-threads.component';
import {DataService} from './services/implementation/dataService';
import { SingleSubforumComponent } from './home/subforum/single-subforum/single-subforum.component';
import { StartThreadComponent } from './home/subforum/topic/start-thread/start-thread.component';
import { StartThreadInputComponent } from './home/subforum/topic/start-thread-input/start-thread-input.component';
import { ThreadPostsComponent } from './home/subforum/topic/thread-posts/thread-posts.component';
import { AddPostComponent } from './home/subforum/topic/thread-posts/add-post/add-post.component';
import { SinglePostComponent } from './home/subforum/topic/thread-posts/single-post/single-post.component';
import { SingleUserMessagesComponent } from './inbox/single-user-messages/single-user-messages.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProfileComponent,
    InboxComponent,
    SubforumComponent,
    TopicComponent,
    RecentThreadsComponent,
    SingleSubforumComponent,
    StartThreadComponent,
    StartThreadInputComponent,
    ThreadPostsComponent,
    AddPostComponent,
    SinglePostComponent,
    SingleUserMessagesComponent,
    NavbarComponent,
    SearchResultsComponent,
    LoginPageComponent,
    RegisterPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
