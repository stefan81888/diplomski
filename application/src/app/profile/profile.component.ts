import { Component, OnInit } from '@angular/core';
import { DataService } from '@app/app/services/implementation/dataService';
import { User } from '@app/app/data-model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  private user: User = new User();
  profilePicURL: string;

  constructor(private dataService: DataService) { 
    this.user = dataService.getLoggedUser();
    this.profilePicURL = this.user.pictureUrl;
  }

  ngOnInit() {
  }

}
