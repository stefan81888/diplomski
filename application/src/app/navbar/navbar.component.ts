import { Component, OnInit } from '@angular/core';
import { DataService } from '@app/app/services/implementation/dataService';
import { User } from '@app/app/data-model';
import { Router, ActivatedRoute, NavigationEnd } from '../../../node_modules/@angular/router';
import {filter} from 'rxjs/operators';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  user: User;
  loginText: string = "";
  query: string = ""; 

  constructor(private dataService: DataService, private router:Router, private route: ActivatedRoute) { }

  ngOnInit() {
      this.loginText = this.dataService.isLogged() ? "Logout" : "Login";
  }

  isLogged(): boolean{
    return this.dataService.isLogged();
  }

  search(){
    if(this.query === "")
      return;

    var criteria = this.query;
    this.query = "";
    this.router.navigate(['search-results', criteria]);
  }

  login(){
    if(!this.dataService.isLogged())
      this.router.navigate(['login']);
      else {
        this.dataService.logout();
        this.loginText = "Login";
      };

     this. router.events.pipe(
        filter(event => event instanceof NavigationEnd)  
      ).subscribe((event: NavigationEnd) => {
      this.loginText = this.dataService.isLogged() ? "Logout" : "Login";
      }); 
  }

}
